package user;
	interface user{  
	void msg();  
	}  
	//Implementation: by second user  
	class admin implements user{  
	public void msg(){
		System.out.println("admin:hello world");
		}
	}
class normaluser extends admin{
public void msg1() {
	System.out.println("normal user:hello world");
}
}
	
	//Using interface: by third user  
	public class TestInterface1{  
	public static void main(String args[]){ 
		System.out.println(" by main :hello world");
	 admin a=new admin();//In real scenario, object is provided by method e.g. getDrawable()  
	a.msg(); 
	normaluser n=new normaluser();
	n.msg1();
	}}  

